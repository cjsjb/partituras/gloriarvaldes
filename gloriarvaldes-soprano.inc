\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble"
		\key e \minor

		R1*2  |
		e' 4 e' 8 e' 4 fis' 8 g' g' ~  |
		g' 8 e' ~ e' 2 r4  |
%% 5
		fis' 4 fis' 8 g' 4 a' b' 8 ~  |
		b' 2 r4 b' 8 b'  |
		b' 4 fis' 8 fis' a' a' g' fis'  |
		g' 2. r4  |
		e' 4 e' 8 fis' 4 g' 4.  |
%% 10
		g' 4. fis' fis' 8 fis'  |
		fis' 4. e' 4 e' 8 e' fis'  |
		e' 4 d' 4. r8 d' d'  |
		e' 4 e' e' 8 e' e' e'  |
		fis' 4 fis' r8 fis' e' fis'  |
%% 15
		g' 4 g' 4. r8 r g'  |
		a' 4 a' \times 2/3 { a' g' a' }  |
		b' 2. r4  |
		b' 2 a' 4 g'  |
		fis' 4 e' fis' g'  |
%% 20
		e' 4. e' r4  |
		fis' 4 fis' 8 fis' 4 fis' 8 fis' 4  |
		r2 a' 4 g'  |
		fis' 4. g' 8 ~ g' 2 ~  |
		g' 2 r  |
%% 25
		e' 8 e' 4 e' 4. r8 e'  |
		fis' 8 fis' fis' fis' 4. r4  |
		g' 8 g' g' g' 4 g' r8  |
		c'' 4 c'' 8 c'' 4 c'' 8 b' a'  |
		b' 8 b' b' b' 4 b' r8  |
%% 30
		a' 4 a' 8 a' 4 g' fis' 8  |
		e' 8 ( fis' ) g' 2 r4  |
		c'' 4 c'' 8 c'' 4 c'' 8 b' a'  |
		b' 8 b' b' b' 4 b' 8 r b'  |
		a' 4 a' 8 g' 4 fis' fis' 8 ~  |
%% 35
		fis' 8 e' e' 2. (  |
		fis' 2. ) r4  |
		g' 4 d' 8 g' 4 b' c'' 8 ~  |
		c'' 8 b' a' g' fis' g' a' a' ~  |
		a' 8 g' ~ g' 4 ~ g' r  |
%% 40
		fis' 4 fis' 8 fis' 4 e' fis' 8  |
		g' 8 ( fis' ) g' 4. r8 a' b'  |
		c'' 4 c'' 8 c'' 4 b' 8 a' b' ~  |
		b' 8 b' 2 r8 a' g'  |
		a' 4 a' 8 a' 4 g' 8 fis' 4  |
%% 45
		e' 2 r4 e'  |
		fis' 4 fis' 8 fis' ~ fis' 2  |
		r2 a' 4 g'  |
		fis' 4. g' 8 ~ g' 2 ~  |
		g' 4 r r2  |
%% 50
		c'' 4 c'' 8 c'' c'' b' a' b' ~  |
		b' 8 b' 4. r4 a' 8 g'  |
		a' 8 a' a' a' 4 g' fis' 8  |
		gis' 2 ( a' 4 b' ~  |
		b' 1 )  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-soprano" {
		Glo -- "ria a" Dios en el Cie __ lo, __
		"y en" la tie -- rra paz __
		a los hom -- bres de bue -- na vo -- lun -- tad.
		Por "tu in" -- men -- sa glo -- ria
		"te a" -- la -- ba -- mos, te ben -- de -- ci -- mos,
		"te a" -- do -- ra -- mos, te glo -- ri -- fi -- ca -- mos,
		te da -- mos gra -- cias, Se -- ñor, Dios rey ce -- les -- tial
		Dios, pa -- dre to -- do -- po -- de -- ro -- so
		Hi -- jo ú -- ni -- co, Je -- su -- cris -- to. __
		Se -- ñor, Dios, Cor -- de -- ro de Dios
		hi -- jo del Pa -- dre, tú que qui -- tas el pe -- ca -- do del mun -- do
		ten pie -- dad de no -- so __ tros.
		Tú que qui -- tas el pe -- ca -- do del mun -- do
		a -- tien -- "de a" nues -- tras sú __ pli -- cas. __
		Tú "que es" -- tás sen -- ta __ "do a" la de -- re -- cha del Pa __ dre __
		ten pie -- dad de no -- so __ tros
		por -- que so -- lo tú e -- res san __ to
		so -- lo tú, Se -- ñor, so -- lo tú
		al -- tí -- si -- mo __ Je -- su -- cris -- to. __
		Con el Es -- pí -- ri -- tu San __ to
		en la glo -- ria de Dios pa -- "dre, A" -- mén. __
	}
>>
