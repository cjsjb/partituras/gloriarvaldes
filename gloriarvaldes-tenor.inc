\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble_8"
		\key e \minor

		R1*2  |
		g 4 g 8 g 4 g 8 g g ~  |
		g 8 g ~ g 2 r4  |
%% 5
		a 4 a 8 a 4 a d' 8 ~  |
		d' 2 r4 d' 8 d'  |
		b 4 b 8 b b b b b  |
		b 2. r4  |
		a 4 a 8 a 4 a 4.  |
%% 10
		a 4. a a 8 a  |
		ais 4. ais 4 ais 8 ais ais  |
		b 4 b 4. r8 b b  |
		g 4 g g 8 g g g  |
		a 4 a r8 a a a  |
%% 15
		b 4 b 4. r8 r b  |
		d' 4 d' \times 2/3 { d' d' d' }  |
		dis' 2. r4  |
		c' 2 c' 4 c'  |
		c' 4 c' c' c'  |
%% 20
		b 4. b r4  |
		a 4 a 8 a 4 a 8 a 4  |
		r2 a 4 a  |
		a 4. b 8 ~ b 2 ~  |
		b 2 r  |
%% 25
		g 8 g 4 g 4. r8 g  |
		a 8 a a a 4. r4  |
		b 8 b b b 4 b r8  |
		c' 4 c' 8 d' 4 d' 8 d' d'  |
		d' 8 d' d' e' 4 e' r8  |
%% 30
		a 4 a 8 b 4 b b 8  |
		b 8 ~ b b 2 r4  |
		a 4 a 8 a 4 a 8 a a  |
		a 8 a a b 4 b 8 r b  |
		a 4 a 8 b 4 b a 8 ~  |
%% 35
		a 8 g g 2. (  |
		a 2. ) r4  |
		g' 4 g' 8 g' 4 g' fis' 8 ~  |
		fis' 8 fis' fis' fis' dis' dis' dis' dis' ~  |
		dis' 8 b ~ b 4 ~ b r  |
%% 40
		a 4 a 8 a 4 g a 8  |
		b 8 ( a ) b 4. r8 a b  |
		c' 4 c' 8 d' 4 d' 8 a b ~  |
		b 8 b 2 r8 a g  |
		a 4 a 8 b 4 b 8 a 4  |
%% 45
		g 2 r4 g  |
		a 4 a 8 a ~ a 2  |
		r2 a 4 a  |
		a 4. b 8 ~ b 2 ~  |
		b 4 r r2  |
%% 50
		a 4 a 8 b c' d' c' b ~  |
		b 8 b 4. r4 a 8 g  |
		a 8 a a b 4 b b 8  |
		b 2 ( cis' 4 dis'  |
		e' 1 )  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		Glo -- "ria a" Dios en el Cie __ lo, __
		"y en" la tie -- rra paz __
		a los hom -- bres de bue -- na vo -- lun -- tad.
		Por "tu in" -- men -- sa glo -- ria
		"te a" -- la -- ba -- mos, te ben -- de -- ci -- mos,
		"te a" -- do -- ra -- mos, te glo -- ri -- fi -- ca -- mos,
		te da -- mos gra -- cias, Se -- ñor, Dios rey ce -- les -- tial
		Dios, pa -- dre to -- do -- po -- de -- ro -- so
		Hi -- jo ú -- ni -- co, Je -- su -- cris -- to. __
		Se -- ñor, Dios, Cor -- de -- ro de Dios
		hi -- jo del Pa -- dre, tú que qui -- tas el pe -- ca -- do del mun -- do
		ten pie -- dad de no -- so __ tros.
		Tú que qui -- tas el pe -- ca -- do del mun -- do
		a -- tien -- "de a" nues -- tras sú __ pli -- cas. __
		Tú "que es" -- tás sen -- ta __ "do a" la de -- re -- cha del Pa __ dre __
		ten pie -- dad de no -- so __ tros
		por -- que so -- lo tú e -- res san __ to
		so -- lo tú, Se -- ñor, so -- lo tú
		al -- tí -- si -- mo __ Je -- su -- cris -- to. __
		Con el Es -- pí -- ri -- tu San __ to
		en la glo -- ria de Dios pa -- "dre, A" -- mén. __
	}
>>
