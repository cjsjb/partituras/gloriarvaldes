\context Staff = "mezzo" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzo" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble"
		\key e \minor

		R1*2  |
		b 4 b 8 b 4 b 8 b c' ~  |
		c' 8 c' ~ c' 2 r4  |
%% 5
		d' 4 d' 8 e' 4 fis' g' 8 ~  |
		g' 2 r4 g' 8 g'  |
		fis' 4 dis' 8 dis' fis' fis' e' dis'  |
		e' 2. r4  |
		cis' 4 cis' 8 d' 4 e' 4.  |
%% 10
		e' 4. d' d' 8 d'  |
		cis' 4. cis' 4 cis' 8 cis' cis'  |
		b 4 b 4. r8 b b  |
		c' 4 c' c' 8 c' c' c'  |
		d' 4 d' r8 d' c' d'  |
%% 15
		e' 4 e' 4. r8 r e'  |
		fis' 4 fis' \times 2/3 { fis' e' fis' }  |
		fis' 2. r4  |
		e' 2 e' 4 e'  |
		e' 4 e' e' e'  |
%% 20
		b 4. b r4  |
		b 4 d' 8 d' 4 d' 8 d' 4  |
		r2 fis' 4 e'  |
		d' 4. e' 8 ~ e' 2 ~  |
		e' 2 r  |
%% 25
		b 8 b 4 b 4. r8 b  |
		d' 8 d' d' d' 4. r4  |
		e' 8 e' e' e' 4 e' r8  |
		e' 4 e' 8 fis' 4 fis' 8 g' fis'  |
		g' 8 g' g' g' 4 g' r8  |
%% 30
		c' 4 c' 8 d' 4 e' dis' 8  |
		b 8 ( dis' ) e' 2 r4  |
		d' 4 d' 8 fis' 4 fis' 8 g' fis'  |
		g' 8 g' g' g' 4 g' 8 r g'  |
		e' 4 e' 8 e' 4 dis' c' 8 ~  |
%% 35
		c' 8 c' c' 2. (  |
		d' 2. ) r4  |
		d' 4 b 8 d' 4 g' b 8 ~  |
		b 8 b b e' dis' e' fis' fis' ~  |
		fis' 8 e' ~ e' 4 ~ e' r  |
%% 40
		d' 4 d' 8 d' 4 c' d' 8  |
		d' 8 ( c' ) d' 4. r8 d' d'  |
		e' 4 e' 8 fis' 4 g' 8 fis' g' ~  |
		g' 8 g' 2 r8 e' e'  |
		e' 4 e' 8 dis' 4 e' 8 dis' 4  |
%% 45
		c' 2 r4 c'  |
		d' 4 d' 8 d' ~ d' 2  |
		r2 fis' 4 e'  |
		d' 4. e' 8 ~ e' 2 ~  |
		e' 4 r r2  |
%% 50
		d' 4 d' 8 d' fis' g' fis' g' ~  |
		g' 8 g' 4. r4 a' 8 g'  |
		e' 8 e' e' e' 4 cis' dis' 8  |
		e' 2 ( ~ e' 4 fis'  |
		gis' 1 )  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzo" {
		Glo -- "ria a" Dios en el Cie __ lo, __
		"y en" la tie -- rra paz __
		a los hom -- bres de bue -- na vo -- lun -- tad.
		Por "tu in" -- men -- sa glo -- ria
		"te a" -- la -- ba -- mos, te ben -- de -- ci -- mos,
		"te a" -- do -- ra -- mos, te glo -- ri -- fi -- ca -- mos,
		te da -- mos gra -- cias, Se -- ñor, Dios rey ce -- les -- tial
		Dios, pa -- dre to -- do -- po -- de -- ro -- so
		Hi -- jo ú -- ni -- co, Je -- su -- cris -- to. __
		Se -- ñor, Dios, Cor -- de -- ro de Dios
		hi -- jo del Pa -- dre, tú que qui -- tas el pe -- ca -- do del mun -- do
		ten pie -- dad de no -- so __ tros.
		Tú que qui -- tas el pe -- ca -- do del mun -- do
		a -- tien -- "de a" nues -- tras sú __ pli -- cas. __
		Tú "que es" -- tás sen -- ta __ "do a" la de -- re -- cha del Pa __ dre __
		ten pie -- dad de no -- so __ tros
		por -- que so -- lo tú e -- res san __ to
		so -- lo tú, Se -- ñor, so -- lo tú
		al -- tí -- si -- mo __ Je -- su -- cris -- to. __
		Con el Es -- pí -- ri -- tu San __ to
		en la glo -- ria de Dios pa -- "dre, A" -- mén. __
	}
>>
