\context ChordNames
	\chords {
		\set majorSevenSymbol = \markup { "maj7" }
		\set chordChanges = ##t

		% intro
		e1:m e1:m

		% gloria a dios en el cielo...
		e1:m c1 d1 g1 b1 e1:m
		a1 d1 fis1 b1:m
		c1 d1 e1:m d1 b1

		c1 c1 e1:m d1 r1
		e1:m e1:m

		e1:m d1 e1:m
		a2:m d2 g2 e2:m
		a2:m b2 e1:m

		a2:m d2 g2 e2:m
		a2:m b2 c1 d1:7

		% tu que estas sentado...
		g1 a2:m/fis b2 e1:m
		d1 g1
		a2:m d2 g2 e2:m a2:m b2
		c1 d1 r1 e1:m e1:m

		a2:m d2 g2 e2:m a2:m b2
		e2 a4 b4 e1
	}
